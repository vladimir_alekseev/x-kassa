$(function(){
	designSelect.init()
	
	$('input, textarea').placeholder();	
});

var designSelect = {
	init: function(){
		var _self = this
		$(document).on("change","div.select select",function(){
			$(this).next("input").val($(this).find("option:selected").html())
		})
		$(document).bind('DOMNodeInserted, DOMNodeRemoved',function(){
			_self.initialise()
		})
		_self.initialise()
	},
	initialise: function(){
		$("div.select select").filter(function( index ) {
			return $(this).parent().find("input").length === 0;
		}).each(function(){
			if($(this).parent().find("input").length == 0){
				$(this).parent().append("<input type='text' disabled>")
				$(this).trigger("change")
			}
		})
	}
}


